#!/usr/bin/env bash
#backdir is path to dropbox backup directory ("~/backups/sombraspace/SN_"curend year-month-day)
backdir=$HOME/Dropbox/backups/sombraspace/SSL_`date '+%Y-%m-%d'`/images
mkdir -p $backdir

# need to change to current project path
# tar -zcvf $backdir/images_`date '+%Y-%m-%d_%H-%M-%S'`.tar.gz -C $HOME/IdeaProjects/sombraspace/attachment/ .
find $HOME/IdeaProjects/sombraspace/attachment/ -printf "%P\n" -type f -o -type l -o -type d | tar -czvf $backdir/images_`date '+%Y-%m-%d_%H-%M-%S'`.tar.gz --no-recursion -C $HOME/IdeaProjects/sombraspace/attachment/ -T -