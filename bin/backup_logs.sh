#!/usr/bin/env bash
#backdir is path to dropbox backup directory ("~/backups/sombraspace/SN_"curend year-month-day)
backdir=$HOME/Dropbox/backups/sombraspace/SSL_`date '+%Y-%m-%d'`/logs
mkdir -p $HOME/backups/logs
touch $HOME/backups/logs/ sombraspace.log
mkdir -p $backdir  
find $HOME/backups/logs/ -printf "%P\n" -type f -o -type l -o -type d | tar -czvf $backdir/log`date '+%Y-%m-%d_%H-%M-%S'`.tar.gz --no-recursion -C $HOME/backups/logs/ -T -