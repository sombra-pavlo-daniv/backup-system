Thanks for opening this file.

Visit this doc to get detailed user guide.
https://docs.google.com/document/d/1fg-20kOU_1HvclgRESU78L0TXzKaUR9oafqlEkZMLqY/edit

Found a bug? ran into a problem? send a message here: pavlo.daniv@sombrainc.com

!!!!!  IMPORTANT !!!!!
Files backup_images.sh, backup_logs.sh and rm_old_files.sh should be used as long as you can see this message.