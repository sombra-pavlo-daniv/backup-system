#!/usr/bin/env bash
backdir=$HOME/backup-system/myproject/MPR_`date '+%Y-%m-%d'`/db
mkdir -p $backdir
mysqldump -u <user> -p<pass> --databases <databases names> | gzip > $backdir/MPR_`date '+%Y-%m-%d_%H-%M-%S'`.sql.gz